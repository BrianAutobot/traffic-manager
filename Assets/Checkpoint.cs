﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkpoint : MonoBehaviour {

	public List <GameObject> CheckpointOptions;

	public GameObject mySpawnRayObj;
	public bool isExit;

	public bool isLeftTurn;
	public bool isRightTurn;
	public bool isStraight;
	public bool isSpawn;
	public bool CanSpawn;
	public float SpawnGapMin;
	public float SpawnGapMax;
	public float TimeOfNextSpawn;

	void Start(){
		CanSpawn = true;
		TimeOfNextSpawn = Time.time + (Random.Range (SpawnGapMin,SpawnGapMax));
	}
	void Update(){
		if (Time.time >= TimeOfNextSpawn) {
			CanSpawn = true;
		}
	}
}
