﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewCar : MonoBehaviour
{

	[Header ("CAR PARTS")]
	public GameObject FrontRaySpawn;
	public GameObject LeftTurnSignal;
	public GameObject RightTurnSignal;
	public GameObject WarningObj;
	public GameObject AngryObj;
	public GameObject HeadLight;

	public Image TopImage;
	[Header ("CAR SETTINGS")]
	public float MaxSpeed;
	public float Accel;
	public float turningspeed;
	public float breakingSpeed;
	public float SlowDownSpeed;
	public float currentSpeed;

	public float MaxAngerLevel;
	public float WaitTillMad;
	public float CurrentAnger;

	public LayerMask CarMask;

	public Sprite LeftImage;
	public Sprite RightImage;
	[Header ("RUNTIME SHIT")]
	public GameObject MyCheckpoint;
	public bool HasCheckpoint;
	public GameObject myNextCheckpoint;

	public bool SlowDown;
	public bool StopForRedLight;
	public bool StopForCar;
	public bool turningLeft;
	public bool turningRight;
	public bool notTurning;
	public bool isStopped;

	public float CurrentStoppedTime;

	void Start ()
	{	
		if (TopImage.gameObject.activeInHierarchy == true) {
			TopImage.gameObject.SetActive (false);
		}
		if (LeftTurnSignal.activeInHierarchy == true) {
			LeftTurnSignal.SetActive (false);
		}
		if (RightTurnSignal.activeInHierarchy == true) {
			RightTurnSignal.SetActive (false);
		}	
		StopForCar = false;
		StopForRedLight = false;
		currentSpeed = 0f;
		SlowDown = false;
	}

	public IEnumerator TurnSignalOff ()
	{
		yield return new WaitForSeconds (2f);
		turningLeft = false;
		turningRight = false;
		yield break;

	}

	void FixedUpdate ()
	{
		if (GameMaster.instance.isPaused == false) {
			if (CurrentAnger < MaxAngerLevel) {
				CheckFrontRay ();
			} else {
				StopForCar = false;
				StopForRedLight = false;
			}
		}

		if (GameMaster.instance.myGameClock.CurrentTime >= 700f && GameMaster.instance.myGameClock.CurrentTime <= 1800f) {
			if (HeadLight.activeInHierarchy == true) {
				HeadLight.SetActive (false);
			}
		} else {
			if (HeadLight.activeInHierarchy == false) {
				HeadLight.SetActive (true);
			}
		}
	}

	void Update ()
	{
		if (GameMaster.instance.isPaused == false) {
			if (HasCheckpoint == true) {
				Vector3 newLook = Vector3.Lerp (transform.position, MyCheckpoint.transform.position, turningspeed);
				transform.LookAt (newLook);
			}
			isStopped = false;
			//set current speed
			if (StopForCar == true || StopForRedLight == true) {
				if (currentSpeed > 0f) {				
					CurrentStoppedTime = 0f;
					currentSpeed -= breakingSpeed;
					if (currentSpeed <= 0f) {
						currentSpeed = 0f;
					}
				} else {
					isStopped = true;
				}
			} else if (SlowDown == true) {
				if (currentSpeed > SlowDownSpeed) {
					currentSpeed -= breakingSpeed;
					if (currentSpeed < SlowDownSpeed) {
						currentSpeed = SlowDownSpeed;
					}
				}
			} else {
				if (currentSpeed < MaxSpeed) {
					currentSpeed += Accel;
				}
				if (currentSpeed > MaxSpeed) {
					currentSpeed = MaxSpeed;
				}
			}

			if (MaxSpeed != 0f && Mathf.Abs (currentSpeed) > .2f) {
				transform.Translate (Vector3.forward * currentSpeed * Time.deltaTime);
			}

			//turn

			Checkpoint nextCP = myNextCheckpoint.GetComponent<Checkpoint> ();
			if (nextCP.isLeftTurn == true) {
				turningLeft = true;
				if (TopImage.gameObject.activeInHierarchy == false) {
					TopImage.gameObject.SetActive (true);
					TopImage.sprite = LeftImage;
				}
				if (LeftTurnSignal.activeInHierarchy == false) {
					LeftTurnSignal.SetActive (true);
				}
			}
			if (nextCP.isRightTurn == true) {
				turningRight = true;
				if (TopImage.gameObject.activeInHierarchy == false) {
					TopImage.gameObject.SetActive (true);
					TopImage.sprite = RightImage;
				}
				if (RightTurnSignal.activeInHierarchy == false) {
					RightTurnSignal.SetActive (true);
				}
			}
			if (nextCP.isStraight == true) {
				
			}

			//anger

			if (isStopped == true) {
				CurrentStoppedTime += Time.deltaTime;
				if (CurrentStoppedTime >= WaitTillMad) {				
					CurrentAnger += Time.deltaTime;
					if (CurrentAnger >= MaxAngerLevel) {
						if (AngryObj.activeInHierarchy == false) {
							AngryObj.SetActive (true);
						}
					} else if (WarningObj.activeInHierarchy == false) {
						WarningObj.SetActive (true);
					}
				} else {
					if (WarningObj.activeInHierarchy == true) {
						WarningObj.SetActive (false);
					}
					if (AngryObj.activeInHierarchy == true) {
						AngryObj.SetActive (false);
					}
				}
			} else {
				if (WarningObj.activeInHierarchy == true) {
					WarningObj.SetActive (false);
				}
				if (AngryObj.activeInHierarchy == true) {
					AngryObj.SetActive (false);
				}
			}
		}
	}

	void OnTriggerEnter (Collider col)
	{
		if (HasCheckpoint == true) {
			if (col.gameObject == MyCheckpoint) {
				//get new checkpoint at random from options in myCheckpoint
				Checkpoint thisCP = col.gameObject.GetComponent<Checkpoint> ();
				if (thisCP.isRightTurn == true || thisCP.isLeftTurn == true) {
					StartCoroutine (TurnSignalOff ());
				}
				//check if exit
				if (thisCP.isExit == true) {
					//game master remove me!
					GameMaster.instance.CarGotOut (this.gameObject);
				} else {
					//pick new checkpoint in advance
					Checkpoint OptionPool = myNextCheckpoint.GetComponent<Checkpoint> ();
					GameObject newCheckpointChoice = myNextCheckpoint;
					if (OptionPool.CheckpointOptions.Count > 1) {
						newCheckpointChoice = OptionPool.CheckpointOptions [Random.Range (0, OptionPool.CheckpointOptions.Count)];
					} else if (OptionPool.CheckpointOptions.Count == 1) {
						newCheckpointChoice = OptionPool.CheckpointOptions [0];
					} else if (OptionPool.isExit != true) {
						//Debug.Log ("Checkpoint has no options!!!");
						transform.Translate (Vector3.up * 3f); //lol
					}

					MyCheckpoint = myNextCheckpoint;
					myNextCheckpoint = newCheckpointChoice;

					//Debug.Log (gameObject.name.ToString () + " has reached its checkpoint, is now headed to " + MyCheckpoint.name.ToString ());
				}
			} else {
				//check for collisions, red lights etc

				if (col.gameObject.tag == "car") {
					//Debug.Log ("GAME OVER" + gameObject.name.ToString ());
					GameMaster.instance.GameOver ();
				}
			} 
		} else {
			//Debug.Log ("Something wrong with spawn");
		}
	}

	public void CheckFrontRay ()
	{
		StopForRedLight = false;
		StopForCar = false;
		SlowDown = false;

		RaycastHit hitinfo = new RaycastHit ();
		if (Physics.Raycast (FrontRaySpawn.transform.position, FrontRaySpawn.transform.forward, out hitinfo, 3.5f, CarMask.value)) {
			//Debug.Log (hitinfo.collider.gameObject.name.ToString ());
			if (hitinfo.collider.gameObject.tag == "Stop") {
				StopForRedLight = true;
			}
			if (hitinfo.collider.gameObject.tag == "Slow") {
				SlowDown = true;
			}
			if (hitinfo.collider.gameObject.tag == "RearBumper") {
				NewCar rayCar = hitinfo.collider.gameObject.transform.parent.gameObject.GetComponent<NewCar> ();
				if (rayCar != null) {
					if (rayCar.StopForRedLight == true || rayCar.StopForCar == true || rayCar.turningLeft == true || rayCar.turningRight == true || rayCar.SlowDown == true || rayCar.currentSpeed < currentSpeed) {
						StopForCar = true;
					}
					else {
						SoundEffects.instance.PlayRandomCarHorn (this.gameObject.transform.position);
					}
				}
			}
			if (hitinfo.collider.gameObject.tag == "car") {
				NewCar rayCar = hitinfo.collider.gameObject.GetComponent<NewCar> ();
				if (rayCar != null) {
					if (rayCar.StopForRedLight == true || rayCar.StopForCar == true || rayCar.currentSpeed < currentSpeed) {
						StopForCar = true;
					} else {
						SoundEffects.instance.PlayRandomCarHorn (this.gameObject.transform.position);
					}
				}
			}
		}


	}
}
