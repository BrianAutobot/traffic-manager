﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundEffects : MonoBehaviour {

	public GameObject SpeakerPrefab;

	public List<AudioClip> CarHorns;
	public List<AudioClip> CarCrashes;

	public static SoundEffects instance;
	// Use this for initialization
	void Start () {
		instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void PlayRandomCarHorn(Vector3 SoundLocation){
		GameObject newSpeaker = Instantiate (SpeakerPrefab, SoundLocation, Quaternion.identity)as GameObject;

		AudioSource newSpeak = newSpeaker.GetComponent<AudioSource> ();
		newSpeak.clip = CarHorns [Random.Range (0, CarHorns.Count)];
		newSpeak.Play ();
		Destroy (newSpeaker, newSpeak.clip.length + .1f);
	}

	public void PlayRandomCarCrash(Vector3 SoundLocation){
		GameObject newSpeaker = Instantiate (SpeakerPrefab, SoundLocation, Quaternion.identity)as GameObject;

		AudioSource newSpeak = newSpeaker.GetComponent<AudioSource> ();
		newSpeak.clip = CarCrashes [Random.Range (0, CarCrashes.Count)];
		newSpeak.Play ();
		Destroy (newSpeaker, newSpeak.clip.length + .1f);
	}
}
