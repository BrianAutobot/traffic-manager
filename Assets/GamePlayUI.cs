﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayUI : MonoBehaviour {

	public Text CurrentTimeLabel;
	public Text CurrentScoreLabel;
	public Text GameOverLabel;
	public GameObject RushHourNotif;
	// Use this for initialization

	public GameObject MainMenuPanel;
	public GameObject PausePanel;

	void Start () {
		CurrentScoreLabel.text = "";
		CurrentTimeLabel.text = "";
	}
	
	// Update is called once per frame
	void FixedUpdate () {		
		if (GameMaster.instance.isPlaying == true) {

			//CURRENTSCORE
			CurrentScoreLabel.text = "Score: " + GameMaster.instance.CurrentScore.ToString ();

			//Current Time
			CurrentTimeLabel.text = GameMaster.instance.myGameClock.CurrentTimeOfDay();
			//RUSH HOUR NOTIF
			if (GameMaster.instance.RushHour == true && RushHourNotif.activeInHierarchy == false) {
				RushHourNotif.SetActive (true);
			} else if (GameMaster.instance.RushHour == false && RushHourNotif.activeInHierarchy == true) {
				RushHourNotif.SetActive (false);
			}
			//
		}

		if (GameMaster.instance.isPaused == true) {
			if (PausePanel.activeInHierarchy == false) {
				PausePanel.SetActive (true);
			}
		} else {
			if (PausePanel.activeInHierarchy == true) {
				PausePanel.SetActive (false);
			}
		}

		if (GameMaster.instance.GameOverStatus == true && GameOverLabel.gameObject.activeInHierarchy == false) {
			GameOverLabel.gameObject.SetActive (true);
		} else if (GameMaster.instance.GameOverStatus == false && GameOverLabel.gameObject.activeInHierarchy == true) {
			GameOverLabel.gameObject.SetActive (false);
		}

		if (GameMaster.instance.MainMenuStatus == true && MainMenuPanel.activeInHierarchy == false) {
			MainMenuPanel.SetActive (true);
		} else if (GameMaster.instance.MainMenuStatus == false && MainMenuPanel.activeInHierarchy == true) {
			MainMenuPanel.SetActive (false);
		}
	}
}
