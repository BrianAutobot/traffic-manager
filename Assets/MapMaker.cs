﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapMaker : MonoBehaviour {
	
	public float TreeSpawnChance;
	[Tooltip("Tree Prefabs")]
	public List<GameObject> Treefabs;
	public List<GameObject> Buildingfabs;

	public GameObject[] BuildingSpawns;
	public GameObject[] TreeSpawns;

	List<GameObject> SpawnedTrees;
	List<GameObject> SpawnedBuildings;


	public static MapMaker instance;
	// Use this for initialization
	void Start () {		
		instance = this;
		TreeSpawns = GameObject.FindGameObjectsWithTag ("TreeSpawn");
		BuildingSpawns = GameObject.FindGameObjectsWithTag ("BuildingSpawn");
		Debug.Log (TreeSpawns.Length.ToString () + " Tree Spawns Found");
		Debug.Log (BuildingSpawns.Length.ToString () + "Building Spawns Found");
	}

	public void ClearBuildings(){
		if (SpawnedBuildings != null) {
			if (SpawnedBuildings.Count > 0) {
				for (int i = SpawnedBuildings.Count - 1; i > -1; i--) {
					if (SpawnedBuildings [i] != null) {
						Destroy(SpawnedBuildings[i]);
					}
				}
			}
		}
	}

	public void ClearTrees(){
		if (SpawnedTrees != null) {
			if (SpawnedTrees.Count > 0) {
				for (int i = SpawnedTrees.Count - 1; i > -1; i--) {
					if (SpawnedTrees [i] != null) {
						Destroy(SpawnedTrees[i]);
					}
				}
			}
		}
	}

	public void NewBuildings(){
		ClearBuildings ();
		SpawnedBuildings = new List<GameObject> ();
		foreach (GameObject buildingSpawn in BuildingSpawns) {
			int randoNum = Random.Range (0, 100);
			if (randoNum <= 90) {

				GameObject newBuilding = Instantiate (Buildingfabs [Random.Range (0, Buildingfabs.Count - 1)], buildingSpawn.transform.position,Quaternion.identity)as GameObject;
				SpawnedBuildings.Add (newBuilding);
			}
		}
	}

	public void NewTrees(){
		ClearTrees ();
		SpawnedTrees = new List<GameObject> ();
		foreach (GameObject treeSpawn in TreeSpawns) {
			int randoNum = Random.Range (0, 100);
			if (randoNum <= TreeSpawnChance) {
				
				GameObject newTree = Instantiate (Treefabs [Random.Range (0, Treefabs.Count - 1)], treeSpawn.transform.position,Quaternion.identity)as GameObject;
				newTree.transform.eulerAngles = new Vector3 (0f, Random.Range (0f, 360f), 0f);
				SpawnedTrees.Add (newTree);
			}
		}
	}
}
