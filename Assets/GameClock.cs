﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameClock : MonoBehaviour {

	public float CurrentTime;
	public float MaxTime;
	public float TimeMulti;
	public bool TimeTicking;

	public GameObject SunAndMoonHolder;

	// Use this for initialization
	void Start () {
		CurrentTime = 0f;
		TimeTicking = false;

	}
	
	// Update is called once per frame
	void Update () {
		if (TimeTicking == true && GameMaster.instance.isPaused == false) {
			CurrentTime += Time.deltaTime*TimeMulti;

			if (CurrentTime >= MaxTime) {
				CurrentTime = 0f;
			}
			SetSunRotation ();
		}

	}

	public void ResetGameClock(float TimeToSet){
		CurrentTime = TimeToSet;
		TimeTicking = false;
	}

	public void StartGame(float StartTime){
		CurrentTime = StartTime;
		StartClock ();
	}

	public void StartClock(){
		TimeTicking = true;
	}

	public void StopClock(){
		TimeTicking = false;
	}

	public int CurrentTimeOfDayHour(){
		return Mathf.FloorToInt(CurrentTime / 100);
	}
	public int CurrentTimeOfDayMinute(){
		return Mathf.FloorToInt((CurrentTime - (CurrentTimeOfDayHour()*100)) * .6f);
	}

	public string CurrentTimeOfDay(){		

		if (CurrentTimeOfDayMinute() < 10) {
			return "Clock: " + CurrentTimeOfDayHour().ToString () + ":0" + CurrentTimeOfDayMinute().ToString ();
		} else {
			return "Clock: " + CurrentTimeOfDayHour().ToString () + ":" + CurrentTimeOfDayMinute().ToString ();
		}
	}

	public void SetSunRotation(){		
			//temp
		SunAndMoonHolder.transform.eulerAngles = new Vector3(0f,0f,
			(CurrentTimeOfDayHour()*15)	
			+
			15*(CurrentTime-(CurrentTimeOfDayHour()*100))/100
		);
	}
}
