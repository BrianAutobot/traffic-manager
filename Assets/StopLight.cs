﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StopLight : MonoBehaviour {
	public enum State{Green,Yellow,Red};

	public GameObject myColliderHolder;
	public Image myButtonImage;
	public Sprite SptGreen;
	public Sprite SptYellow;
	public Sprite SptRed;

	public GameObject RedLight;
	public GameObject GreenLight;
	public GameObject YellowLight;

	public float YellowDurration;

	public State CurrentState;
	public float TimetoRed;


	// Use this for initialization
	void Start () {
		CurrentState = State.Red;
		GreenLight.SetActive (false);
		YellowLight.SetActive (false);
		RedLight.SetActive (false);
		Cycle ();
	}
	
	// Update is called once per frame
	void Update () {		
		if (CurrentState == State.Yellow) {
			if (GameMaster.instance.isPaused == true) {
				TimetoRed += Time.deltaTime;
			}
			if (Time.time >= TimetoRed) {
				Cycle ();
			}
			myColliderHolder.tag = "Slow";
			this.gameObject.tag = "Slow";
		} else {
			myColliderHolder.tag = "Stop";
			this.gameObject.tag = "Stop";
		}

		if (CurrentState == State.Green) {			
				myColliderHolder.transform.localPosition = Vector3.up * -2;
		} else if (CurrentState == State.Yellow || CurrentState == State.Red) {			
			myColliderHolder.transform.localPosition = Vector3.zero;
		}
	}

	public void Cycle(){
		if (CurrentState == State.Green) {
			CurrentState = State.Yellow;
			TimetoRed = Time.time + YellowDurration;
			myButtonImage.sprite = SptYellow;
			YellowLight.SetActive (true);
			GreenLight.SetActive (false);
		} else if (CurrentState == State.Yellow) {
			CurrentState = State.Red;
			myButtonImage.sprite = SptRed;
			RedLight.SetActive (true);
			YellowLight.SetActive (false);
		} else if (CurrentState == State.Red) {
			CurrentState = State.Green;
			myButtonImage.sprite = SptGreen;
			GreenLight.SetActive (true);
			RedLight.SetActive (false);
		}	
	}
}
