﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Intersection : MonoBehaviour {

	public GameObject BlockSide;
	public GameObject BlockUp;


	public static Intersection instance;

	void Start(){
		instance = this;
	}	

	public void ToggleSide(){
		if (BlockSide.transform.position.y > 3f) {
			BlockSide.transform.position = Vector3.zero;
		} else {
			BlockSide.transform.position = Vector3.up * 10f;
		}
	}

	public void ToggleUp(){
		if (BlockUp.transform.position.y > 3f) {
			BlockUp.transform.position = Vector3.zero;
		} else {
			BlockUp.transform.position = Vector3.up * 10f;
		}
	}
}