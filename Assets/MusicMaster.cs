﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class MusicMaster : MonoBehaviour {

	public AudioClip MenuMusic;
	public AudioClip RushHourMusic;
	public AudioSource MyMusicSpeaker;


	public bool MusicON;

	// Use this for initialization
	void Start () {
		MyMusicSpeaker.clip = MenuMusic;
		MyMusicSpeaker.Play ();
	}
	
	// Update is called once per frame
	void Update () {
		if (GameMaster.instance.RushHour == true && MyMusicSpeaker.clip != RushHourMusic) {
			MyMusicSpeaker.Stop ();
			MyMusicSpeaker.clip = RushHourMusic;
			MyMusicSpeaker.Play ();
		}
		if (GameMaster.instance.RushHour == false && MyMusicSpeaker.clip != MenuMusic) {
			MyMusicSpeaker.Stop ();
			MyMusicSpeaker.clip = MenuMusic;
			MyMusicSpeaker.Play ();
		}
	}
}
