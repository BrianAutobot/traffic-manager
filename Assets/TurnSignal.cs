﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnSignal : MonoBehaviour
{

	public GameObject RightLight;
	public GameObject LeftLight;

	public float BlinkLength;

	public bool LeftOn;
	public bool RightOn;

	float TimeOfNextBlink;
	NewCar myCar;
	// Use this for initialization
	void Start ()
	{
		RightLight.SetActive (false);
		LeftLight.SetActive (false);
		RightOn = false;
		LeftOn = false;
		myCar = this.gameObject.GetComponent<NewCar> ();
		if (myCar == null) {
			Debug.Log ("no Car Scrip Found by Turn Signal");
		}
	}
	
	// Update is called once per frame
	void Update ()
	{
		
		if (myCar != null) {

			//LEFT TURN
			if (myCar.turningLeft == true) {

				if (LeftOn == false) {
					TimeOfNextBlink = Time.time + BlinkLength;
					LeftOn = true;
				}

				if (LeftOn == true) {					
					if (Time.time >= TimeOfNextBlink) {
						if (LeftLight.activeInHierarchy == true) {
							LeftLight.SetActive (false);
						} else {
							LeftLight.SetActive (true);
						}
						TimeOfNextBlink = Time.time + BlinkLength;
					}
				}

			} else {
				LeftOn = false;
				if (LeftLight.activeInHierarchy == true) {
					LeftLight.SetActive (false);
				}
			}

			//RIGHT TURN
			if (myCar.turningRight == true) {
				
				if (RightOn == false) {
					TimeOfNextBlink = Time.time + BlinkLength;
					RightOn = true;
				}

				if (RightOn == true) {					
					if (Time.time >= TimeOfNextBlink) {
						if (RightLight.activeInHierarchy == true) {
							RightLight.SetActive (false);
						} else {
							RightLight.SetActive (true);
						}
						TimeOfNextBlink = Time.time + BlinkLength;
					}
				}

			} else {
				RightOn = false;
				if (RightLight.activeInHierarchy == true) {
					RightLight.SetActive (false);
				}
			}		

		}

	}


}
